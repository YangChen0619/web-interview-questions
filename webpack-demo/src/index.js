class People {
    constructor (name) {
        this.name = name;
    }
    eat () {
        console.log(`${this.name} eat a something`)
    }
}

class Student extends People {
    constructor (name,number) {
        super(name)
        this.number = number;
    }
}