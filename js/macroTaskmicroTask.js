const $p1 = document.createElement('p')
$p1.innerHTML = '111111'
const $p2 = document.createElement('p')
$p2.innerHTML = '222222'
const $p3 = document.createElement('p')
$p3.innerHTML = '333333'
document.body.appendChild($p1)
document.body.appendChild($p2)
document.body.appendChild($p3)
Promise.resolve().then(()=>{
    console.log(document.getElementsByTagName('p').length)
    alert('promise 微任务 w3c规范 js执行栈清空后执行 在dom渲染之前')
})

setTimeout(()=>{
    console.log(document.getElementsByTagName('p').length)
    alert('setTimeout 宏任务 浏览器规范 在dom渲染之后')
})