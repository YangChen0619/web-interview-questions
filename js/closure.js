// console.log('闭包')

// function create () {
//     const a = 100;
//     return function (){
//         console.log(a)
//     }
// }

// const fn = create();
// const a = 200;
// fn()


// function fn(){
//     console.log(a)
// }
// function print(fn){
//     const a = 200;
//     fn()
// }

// const a = 100;

// print(fn)

for(var i =0;i<10;i++){
    let a = document.createElement('a');
    a.innerHTML = i;
    document.body.append(a);
    a.addEventListener('click',(function(i){
        return function () {
            alert(i)
        }
    })(i))
}

Function.prototype.bind1 = function () {
    // const args = Array.prototype.slice.call(arguments);
    // return function () {
    //     return this.apply(args.shift(),...args)
    // }
    const [first,...args] = Array.prototype.slice.call(arguments);
    return () =>{
        return this.apply(first,args)
    }
}

function fn(){
    console.log(arguments)
    console.log(this)
}
const fn2 = fn.bind1({x:100},1,2,3,4)

fn2()

const obj = {
    a:1,
    b:{
        fn:function(){
            console.log(this.a)
        }
    }
}
var fn = obj.b.fn;
obj.b.fn()