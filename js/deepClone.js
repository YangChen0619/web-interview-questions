let obj1 = {
    age:20,
    name:'yc',
    address:{
        city:'bj'
    },
    arr:[1,2,3],
    date:new Date()
}

let obj2 = deepClone(obj1);
obj2.address.city = 'sh'

console.log(obj1.address.city)
console.log(obj2)
function deepClone (obj = {}) {
    // 如果不是引用类型 或者是null 直接返回
    if(typeof obj !== 'object' || obj == null){
        return obj
    }
    let result //初始化
    if(obj instanceof Array){ // isArray constructor
        result = []
    }else{
        result = {}
    }
    for(key in obj){
        // 判断是不是原型属性
        if(obj.hasOwnProperty(key)){
            result[key] = deepClone(obj[key])
        }
    }
    return result
}