class People {
    constructor (name) {
        this.name = name;
    }
    eat () {
        console.log(`${this.name} eat something`)
    }
}

class Student extends People {
    constructor (name,number) {
        super(name)
        this.number = number;
    }
}

// let yc = new Student('yc',100)


// // f 是 F的实例 f.__proto__ === F.prototype F.prototype.__proto__ === Object.prototype
// // F 是 Function的实例 F.__proto
// // People是Function的实例
// // People是Object的子类

// //          Function 是Object的子类
// console.log(Function instanceof Object)
// console.log(Function.prototype.__proto__=== Object.prototype)


// //          People 是Object的子类
// console.log(People instanceof Object)
// console.log(People.prototype.__proto__=== Object.prototype)

// //          People 是Function的实例
// console.log(People.__proto__=== Function.prototype)
// console.log(People.__proto__.__proto__=== Object.prototype)


// console.log(Student.prototype.__proto__ === People.prototype)


// console.log(Student.__proto__ === People)
// console.log(Student.prototype.__proto__ === People.prototype)
// console.log(yc instanceof Object)
// console.log(Student instanceof Function)
// console.log(People instanceof Function)
// console.log(yc instanceof Student)
// console.log(Student instanceof Function)
// console.log(yc instanceof Function)
// console.log(yc.__proto__)

// 实例  显式原型 隐式原型 构造函数 子类


function Person() {}
person = new Person();

console.log(person.__proto__ === Person.prototype);
console.log(Object.getPrototypeOf(person) === Person.prototype);
console.log(person.constructor === Person); // 注①
console.log(Person.prototype.__proto__ === Object.prototype); //注②
console.log(Object.prototype.__proto__ === null);
console.log(Person.prototype.constructor === Person);
console.log(Object.__proto__ === Function.prototype); //注③
console.log(Function.prototype.__proto__ === Object.prototype);

console.log(Person.prototype.__proto__ === Object.prototype)
console.log(Person.__proto__ === Function.prototype)
console.log(Function.prototype.__proto__ === Object.prototype)
console.log(Array.prototype.__proto__ === Object.prototype)
console.log(Array.__proto__ === Function.prototype)
// 实例的__proto__ 是其构造函数的prototype 
// Person.prototype 是对象 其构造函数是 Object
// Person 是函数 其构造对象是Function  Function是构造函数  普通函数  是JS内置对象
// 子类的原型是父类的实例  子类的prototype===父类的实例 父类实例的__proto__ === 其构造函数的prototype
// 子类的prototype.__proto__ === 父类的prototype
// A instance B A顺着__proto__网上找  看看能不能找到 B的显示原型